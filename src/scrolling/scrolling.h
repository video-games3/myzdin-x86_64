#ifndef SCROLL_H
#define SCROLL_H

void SceneScrolling(struct Scene* scene, struct Player player);
void GroundScrolling(struct Ground* ground, struct Player player);

#endif // SCROLL_H
