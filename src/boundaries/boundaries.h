#ifndef BOUNDARIES_H
#define BOUNDARIES_H

void SceneBoundaries(struct Scene* scene);
void GroundBoundaries(struct Ground* ground);
void PlayerBoundaries(struct Player* player, int screen_width);

#endif //BOUNDARIES_H
